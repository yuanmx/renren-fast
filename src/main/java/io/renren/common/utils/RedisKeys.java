package io.renren.common.utils;

/**
 * Redis所有Keys
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-07-18 19:51
 */
public class RedisKeys {

    public static String getCliConfigKey(String key){
        return "cli:config:" + key;
    }

    public static String getSysConfigKey(String key){
        return "sys:config:" + key;
    }
}
