package io.renren.common.aspect;

import com.google.gson.Gson;
import io.renren.common.annotation.CliLog;
import io.renren.common.utils.HttpContextUtils;
import io.renren.common.utils.IPUtils;
import io.renren.modules.app.entity.UserEntity;
import io.renren.modules.cli.entity.CliLogEntity;
import io.renren.modules.cli.entity.CliUserEntity;
import io.renren.modules.cli.service.CliLogService;
import io.renren.modules.sys.entity.SysUserEntity;
import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;


/**
 * 系统日志，切面处理类
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017年3月8日 上午11:07:35
 */
@Aspect
@Component
public class CliLogAspect {
	@Autowired
	private CliLogService cliLogService;

	@Pointcut("@annotation(io.renren.common.annotation.CliLog)")
	public void logPointCut() {

	}

	@Around("logPointCut()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		long beginTime = System.currentTimeMillis();
		//执行方法
		Object result = point.proceed();
		//执行时长(毫秒)
		long time = System.currentTimeMillis() - beginTime;

		//保存日志
		saveCliLog(point, time);

		return result;
	}

	private void saveCliLog(ProceedingJoinPoint joinPoint, long time) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();

		CliLogEntity cliLogEntity = new CliLogEntity();
		CliLog cliLog = method.getAnnotation(CliLog.class);
		if(cliLog != null){
			//注解上的描述
			cliLogEntity.setOperation(cliLog.value());
		}

		//请求的方法名
		String className = joinPoint.getTarget().getClass().getName();
		String methodName = signature.getName();
		cliLogEntity.setMethod(className + "." + methodName + "()");

		//请求的参数
		Object[] args = joinPoint.getArgs();
		try{
			String params = new Gson().toJson(args[0]);
			cliLogEntity.setParams(params);
		}catch (Exception e){

		}

		//获取request
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		//设置IP地址
		cliLogEntity.setIp(IPUtils.getIpAddr(request));

		//用户名
		String username = ((CliUserEntity) SecurityUtils.getSubject().getPrincipal()).getUsername();
		cliLogEntity.setUsername(username);

		cliLogEntity.setTime(time);
		cliLogEntity.setCreateDate(new Date());
		//保存系统日志
		cliLogService.insert(cliLogEntity);
	}
}
