package io.renren.modules.cli.controller;

import io.renren.common.utils.R;
import io.renren.modules.cli.entity.CliUserEntity;
import io.renren.modules.cli.form.CliLoginForm;
import io.renren.modules.cli.service.CliCaptchaService;
import io.renren.modules.cli.service.CliUserService;
import io.renren.modules.cli.service.CliUserTokenService;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

/**
 * 登录相关
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年11月10日 下午1:15:31
 */
@RestController
public class CliLoginController extends AbstractController {
	@Autowired
	private CliUserService cliUserService;
	@Autowired
	private CliUserTokenService cliUserTokenService;
	@Autowired
	private CliCaptchaService cliCaptchaService;

	/**
	 * 验证码
	 */
	@GetMapping("clicaptcha.jpg")
	public void captcha(HttpServletResponse response, String uuid)throws ServletException, IOException {
		response.setHeader("Cache-Control", "no-store, no-cache");
		response.setContentType("image/jpeg");

		//获取图片验证码
		BufferedImage image = cliCaptchaService.getCaptcha(uuid);

		ServletOutputStream out = response.getOutputStream();
		ImageIO.write(image, "jpg", out);
		IOUtils.closeQuietly(out);
	}

	/**
	 * 登录
	 */
	@PostMapping("/cli/login")
	public Map<String, Object> login(@RequestBody CliLoginForm form)throws IOException {
		boolean captcha = cliCaptchaService.validate(form.getUuid(), form.getCaptcha());
		if(!captcha){
			return R.error("验证码不正确");
		}

		//用户信息
		CliUserEntity user = cliUserService.queryByUserName(form.getUsername());

		//账号不存在、密码错误
		if(user == null || !user.getPassword().equals(new Sha256Hash(form.getPassword(), user.getSalt()).toHex())) {
			return R.error("账号或密码不正确");
		}

		//账号锁定
		if(user.getStatus() == 0){
			return R.error("账号已被锁定,请联系管理员");
		}

		//生成token，并保存到数据库
		R r = cliUserTokenService.createToken(user.getUserId());
		return r;
	}


	/**
	 * 退出
	 */
	@PostMapping("/cli/logout")
	public R logout() {
		cliUserTokenService.logout(getUserId());
		return R.ok();
	}

}
