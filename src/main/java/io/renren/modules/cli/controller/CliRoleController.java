package io.renren.modules.cli.controller;

import io.renren.common.annotation.CliLog;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.cli.entity.CliRoleEntity;
import io.renren.modules.cli.service.CliRoleMenuService;
import io.renren.modules.cli.service.CliRoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 角色管理
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年11月8日 下午2:18:33
 */
@RestController
@RequestMapping("/cli/role")
public class CliRoleController extends AbstractController {
	@Autowired
	private CliRoleService cliRoleService;
	@Autowired
	private CliRoleMenuService cliRoleMenuService;

	/**
	 * 角色列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("cli:role:list")
	public R list(@RequestParam Map<String, Object> params){
		//如果不是超级管理员，则只查询自己创建的角色列表
		if(getUserId()!=null && getUserId()!= Constant.SUPER_ADMIN){
			params.put("createUserId", getUserId());
		}

		PageUtils page = cliRoleService.queryPage(params);

		return R.ok().put("page", page);
	}
	
	/**
	 * 角色列表
	 */
	@GetMapping("/select")
	@RequiresPermissions("cli:role:select")
	public R select(){
		Map<String, Object> map = new HashMap<>();
		
		//如果不是超级管理员，则只查询自己所拥有的角色列表
		if(getUserId() != Constant.SUPER_ADMIN){
			map.put("createUserId", getUserId());
		}
		List<CliRoleEntity> list = cliRoleService.selectByMap(map);
		
		return R.ok().put("list", list);
	}
	
	/**
	 * 角色信息
	 */
	@GetMapping("/info/{roleId}")
	@RequiresPermissions("cli:role:info")
	public R info(@PathVariable("roleId") Long roleId){
        CliRoleEntity role = cliRoleService.selectById(roleId);
		
		//查询角色对应的菜单
		List<Long> menuIdList = cliRoleMenuService.queryMenuIdList(roleId);
		role.setMenuIdList(menuIdList);
		
		return R.ok().put("role", role);
	}
	
	/**
	 * 保存角色
	 */
	@CliLog("保存角色")
	@PostMapping("/save")
	@RequiresPermissions("cli:role:save")
	public R save(@RequestBody CliRoleEntity role){
		ValidatorUtils.validateEntity(role);
		
		role.setCreateUserId(getUserId());
        cliRoleService.save(role);
		
		return R.ok();
	}
	
	/**
	 * 修改角色
	 */
	@CliLog("修改角色")
	@PostMapping("/update")
	@RequiresPermissions("cli:role:update")
	public R update(@RequestBody CliRoleEntity role){
		ValidatorUtils.validateEntity(role);
		
		role.setCreateUserId(getUserId());
        cliRoleService.update(role);
		
		return R.ok();
	}
	
	/**
	 * 删除角色
	 */
	@CliLog("删除角色")
	@PostMapping("/delete")
	@RequiresPermissions("cli:role:delete")
	public R delete(@RequestBody Long[] roleIds){
        cliRoleService.deleteBatch(roleIds);
		return R.ok();
	}
}
