package io.renren.modules.cli.controller;

import io.renren.common.annotation.CliLog;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.Assert;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.cli.entity.CliUserEntity;
import io.renren.modules.cli.service.CliUserRoleService;
import io.renren.modules.cli.service.CliUserService;
import io.renren.modules.cli.form.PasswordForm;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 系统用户
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年10月31日 上午10:40:10
 */
@RestController
@RequestMapping("/cli/user")
public class CliUserController extends AbstractController {
	@Autowired
	private CliUserService cliUserService;
	@Autowired
	private CliUserRoleService cliUserRoleService;


	/**
	 * 所有用户列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("cli:user:list")
	public R list(@RequestParam Map<String, Object> params){
		//只有超级管理员，才能查看所有管理员列表
		if(getUserId() != Constant.SUPER_ADMIN){
			params.put("createUserId", getUserId());
		}
		PageUtils page = cliUserService.queryPage(params);

		return R.ok().put("page", page);
	}
	
	/**
	 * 获取登录的用户信息
	 */
	@GetMapping("/info")
	public R info(){
		return R.ok().put("user", getUser());
	}
	
	/**
	 * 修改登录用户密码
	 */
	@CliLog("修改密码")
	@PostMapping("/password")
	public R password(@RequestBody PasswordForm form){
		Assert.isBlank(form.getNewPassword(), "新密码不为能空");
		
		//sha256加密
		String password = new Sha256Hash(form.getPassword(), getUser().getSalt()).toHex();
		//sha256加密
		String newPassword = new Sha256Hash(form.getNewPassword(), getUser().getSalt()).toHex();
				
		//更新密码
		boolean flag = cliUserService.updatePassword(getUserId(), password, newPassword);
		if(!flag){
			return R.error("原密码不正确");
		}
		
		return R.ok();
	}
	
	/**
	 * 用户信息
	 */
	@GetMapping("/info/{userId}")
	@RequiresPermissions("cli:user:info")
	public R info(@PathVariable("userId") Long userId){
		CliUserEntity user = cliUserService.selectById(userId);
		
		//获取用户所属的角色列表
		List<Long> roleIdList = cliUserRoleService.queryRoleIdList(userId);
		user.setRoleIdList(roleIdList);
		
		return R.ok().put("user", user);
	}
	
	/**
	 * 保存用户
	 */
	@CliLog("保存用户")
	@PostMapping("/save")
	@RequiresPermissions("cli:user:save")
	public R save(@RequestBody CliUserEntity user){
		ValidatorUtils.validateEntity(user, AddGroup.class);
		
		user.setCreateUserId(getUserId());
		cliUserService.save(user);
		
		return R.ok();
	}
	
	/**
	 * 修改用户
	 */
	@CliLog("修改用户")
	@PostMapping("/update")
	@RequiresPermissions("cli:user:update")
	public R update(@RequestBody CliUserEntity user){
		ValidatorUtils.validateEntity(user, UpdateGroup.class);

		user.setCreateUserId(getUserId());
		cliUserService.update(user);
		
		return R.ok();
	}
	
	/**
	 * 删除用户
	 */
	@CliLog("删除用户")
	@PostMapping("/delete")
	@RequiresPermissions("cli:user:delete")
	public R delete(@RequestBody Long[] userIds){
		if(ArrayUtils.contains(userIds, 1L)){
			return R.error("系统管理员不能删除");
		}
		
		if(ArrayUtils.contains(userIds, getUserId())){
			return R.error("当前用户不能删除");
		}
		
		cliUserService.deleteBatch(userIds);
		
		return R.ok();
	}
}
