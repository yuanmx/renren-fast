/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package io.renren.modules.cli.controller;


import io.renren.common.annotation.CliLog;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.cli.entity.CliConfigEntity;
import io.renren.modules.cli.service.CliConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 系统配置信息
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年12月4日 下午6:55:53
 */
@RestController
@RequestMapping("/cli/config")
public class CliConfigController extends AbstractController {
	@Autowired
	private CliConfigService cliConfigService;
	
	/**
	 * 所有配置列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("Cli:config:list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = cliConfigService.queryPage(params);

		return R.ok().put("page", page);
	}
	
	
	/**
	 * 配置信息
	 */
	@GetMapping("/info/{id}")
	@RequiresPermissions("Cli:config:info")
	public R info(@PathVariable("id") Long id){
		CliConfigEntity config = cliConfigService.selectById(id);
		
		return R.ok().put("config", config);
	}
	
	/**
	 * 保存配置
	 */
	@CliLog("保存配置")
	@PostMapping("/save")
	@RequiresPermissions("Cli:config:save")
	public R save(@RequestBody CliConfigEntity config){
		ValidatorUtils.validateEntity(config);

		cliConfigService.save(config);
		
		return R.ok();
	}
	
	/**
	 * 修改配置
	 */
	@CliLog("修改配置")
	@PostMapping("/update")
	@RequiresPermissions("Cli:config:update")
	public R update(@RequestBody CliConfigEntity config){
		ValidatorUtils.validateEntity(config);
		
		cliConfigService.update(config);
		
		return R.ok();
	}
	
	/**
	 * 删除配置
	 */
	@CliLog("删除配置")
	@PostMapping("/delete")
	@RequiresPermissions("Cli:config:delete")
	public R delete(@RequestBody Long[] ids){
		cliConfigService.deleteBatch(ids);
		
		return R.ok();
	}

}
