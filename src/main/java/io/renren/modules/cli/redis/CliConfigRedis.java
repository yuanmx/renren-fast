package io.renren.modules.cli.redis;

import io.renren.common.utils.RedisKeys;
import io.renren.common.utils.RedisUtils;
import io.renren.modules.cli.entity.CliConfigEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统配置Redis
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017/7/18 21:08
 */
@Component
public class CliConfigRedis {
    @Autowired
    private RedisUtils redisUtils;

    public void saveOrUpdate(CliConfigEntity config) {
        if(config == null){
            return ;
        }
        String key = RedisKeys.getCliConfigKey(config.getKey());
        redisUtils.set(key, config);
    }

    public void delete(String configKey) {
        String key = RedisKeys.getCliConfigKey(configKey);
        redisUtils.delete(key);
    }

    public CliConfigEntity get(String configKey){
        String key = RedisKeys.getCliConfigKey(configKey);
        return redisUtils.get(key, CliConfigEntity.class);
    }
}
