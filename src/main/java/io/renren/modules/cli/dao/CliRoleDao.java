package io.renren.modules.cli.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.renren.modules.cli.entity.CliRoleEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 角色管理
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年9月18日 上午9:33:33
 */
@Mapper
public interface CliRoleDao extends BaseMapper<CliRoleEntity> {
	
	/**
	 * 查询用户创建的角色ID列表
	 */
	List<Long> queryRoleIdList(Long createUserId);
}
