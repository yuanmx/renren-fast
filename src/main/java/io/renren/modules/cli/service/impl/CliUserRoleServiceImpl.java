package io.renren.modules.cli.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.renren.common.utils.MapUtils;
import io.renren.modules.cli.dao.CliUserRoleDao;
import io.renren.modules.cli.entity.CliUserRoleEntity;
import io.renren.modules.cli.service.CliUserRoleService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;



/**
 * 用户与角色对应关系
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年9月18日 上午9:45:48
 */
@Service("cliUserRoleService")
public class CliUserRoleServiceImpl extends ServiceImpl<CliUserRoleDao, CliUserRoleEntity> implements CliUserRoleService {

	@Override
	public void saveOrUpdate(Long userId, List<Long> roleIdList) {
		//先删除用户与角色关系
		this.deleteByMap(new MapUtils().put("user_id", userId));

		if(roleIdList == null || roleIdList.size() == 0){
			return ;
		}

		//保存用户与角色关系
		List<CliUserRoleEntity> list = new ArrayList<>(roleIdList.size());
		for(Long roleId : roleIdList){
			CliUserRoleEntity CliUserRoleEntity = new CliUserRoleEntity();
			CliUserRoleEntity.setUserId(userId);
			CliUserRoleEntity.setRoleId(roleId);

			list.add(CliUserRoleEntity);
		}
		this.insertBatch(list);
	}

	@Override
	public List<Long> queryRoleIdList(Long userId) {
		return baseMapper.queryRoleIdList(userId);
	}

	@Override
	public int deleteBatch(Long[] roleIds){
		return baseMapper.deleteBatch(roleIds);
	}
}
