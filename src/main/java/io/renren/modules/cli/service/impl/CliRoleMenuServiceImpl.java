package io.renren.modules.cli.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.renren.modules.cli.dao.CliRoleMenuDao;
import io.renren.modules.cli.entity.CliRoleMenuEntity;
import io.renren.modules.cli.service.CliRoleMenuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;



/**
 * 角色与菜单对应关系
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年9月18日 上午9:44:35
 */
@Service("cliRoleMenuService")
public class CliRoleMenuServiceImpl extends ServiceImpl<CliRoleMenuDao, CliRoleMenuEntity> implements CliRoleMenuService {

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveOrUpdate(Long roleId, List<Long> menuIdList) {
		//先删除角色与菜单关系
		deleteBatch(new Long[]{roleId});

		if(menuIdList.size() == 0){
			return ;
		}

		//保存角色与菜单关系
		List<CliRoleMenuEntity> list = new ArrayList<>(menuIdList.size());
		for(Long menuId : menuIdList){
			CliRoleMenuEntity CliRoleMenuEntity = new CliRoleMenuEntity();
			CliRoleMenuEntity.setMenuId(menuId);
			CliRoleMenuEntity.setRoleId(roleId);

			list.add(CliRoleMenuEntity);
		}
		this.insertBatch(list);
	}

	@Override
	public List<Long> queryMenuIdList(Long roleId) {
		return baseMapper.queryMenuIdList(roleId);
	}

	@Override
	public int deleteBatch(Long[] roleIds){
		return baseMapper.deleteBatch(roleIds);
	}

}
