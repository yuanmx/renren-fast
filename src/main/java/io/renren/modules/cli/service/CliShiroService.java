package io.renren.modules.cli.service;

import io.renren.modules.cli.entity.CliUserEntity;
import io.renren.modules.cli.entity.CliUserTokenEntity;

import java.util.Set;

/**
 * shiro相关接口
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-06-06 8:49
 */
public interface CliShiroService {
    /**
     * 获取用户权限列表
     */
    Set<String> getUserPermissions(long userId);

    CliUserTokenEntity queryByToken(String token);

    /**
     * 根据用户ID，查询用户
     * @param userId
     */
    CliUserEntity queryUser(Long userId);
}
