/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package io.renren.modules.cli.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.gson.Gson;
import io.renren.common.exception.RRException;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.cli.dao.CliConfigDao;
import io.renren.modules.cli.entity.CliConfigEntity;
import io.renren.modules.cli.redis.CliConfigRedis;
import io.renren.modules.cli.service.CliConfigService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Map;

@Service("cliConfigService")
public class CliConfigServiceImpl extends ServiceImpl<CliConfigDao, CliConfigEntity> implements CliConfigService {
	@Autowired
	private CliConfigRedis cliConfigRedis;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String key = (String)params.get("key");

		Page<CliConfigEntity> page = this.selectPage(
				new Query<CliConfigEntity>(params).getPage(),
				new EntityWrapper<CliConfigEntity>()
					.like(StringUtils.isNotBlank(key),"key", key)
					.eq("status", 1)
		);

		return new PageUtils(page);
	}
	
	@Override
	public void save(CliConfigEntity config) {
		this.insert(config);
		cliConfigRedis.saveOrUpdate(config);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(CliConfigEntity config) {
		this.updateById(config);
		cliConfigRedis.saveOrUpdate(config);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateValueByKey(String key, String value) {
		baseMapper.updateValueByKey(key, value);
		cliConfigRedis.delete(key);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteBatch(Long[] ids) {
		for(Long id : ids){
			CliConfigEntity config = this.selectById(id);
			cliConfigRedis.delete(config.getKey());
		}

		this.deleteBatchIds(Arrays.asList(ids));
	}

	@Override
	public String getValue(String key) {
		CliConfigEntity config = cliConfigRedis.get(key);
		if(config == null){
			config = baseMapper.queryByKey(key);
			cliConfigRedis.saveOrUpdate(config);
		}

		return config == null ? null : config.getValue();
	}
	
	@Override
	public <T> T getConfigObject(String key, Class<T> clazz) {
		String value = getValue(key);
		if(StringUtils.isNotBlank(value)){
			return new Gson().fromJson(value, clazz);
		}

		try {
			return clazz.newInstance();
		} catch (Exception e) {
			throw new RRException("获取参数失败");
		}
	}
}
