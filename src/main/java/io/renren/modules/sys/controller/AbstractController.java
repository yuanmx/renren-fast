package io.renren.modules.sys.controller;

import io.renren.modules.cli.entity.CliUserEntity;
import io.renren.modules.sys.entity.SysUserEntity;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller公共组件
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年11月9日 下午9:42:26
 */
public abstract class AbstractController {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	protected SysUserEntity getUser() {
		SysUserEntity entity=null;
		try {
			if(SecurityUtils.getSubject().getPrincipal() instanceof  SysUserEntity){

				entity = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return entity;
	}

	protected Long getUserId() {
		return getUser()==null?null:getUser().getUserId();
	}


}
